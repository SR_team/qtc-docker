#!/usr/bin/bash

docker run --rm -v /tmp:/tmp -v $HOME:/home/$USER -w $(pwd) qtc-docker:latest $@
