FROM srteam/samp-plugin-build:10

RUN groupadd -g 985 user && useradd -ms /bin/bash -g 985 user

USER user

ENTRYPOINT ["/usr/bin/cmake"]
